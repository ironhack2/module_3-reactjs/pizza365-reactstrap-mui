import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import { Container, Row } from "reactstrap";

import Header from "./components/Header";
import HomePage from "./components/content/Homepage";
import Footer from "./components/Footer";

function App() {
  return (
    <div>
      <Row>
        <Header />
      </Row>

      <Container>
        <HomePage />
      </Container>

      <Row className="mt-5">
        <Footer />
      </Row>
    </div>
  );
}

export default App;
