import { Container, Row, Col } from "reactstrap";
import {
  Typography,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
} from "@mui/material";
import { useEffect, useState } from "react";

function Drink({ addDrinkProp }) {
  const [selectDrink, setSelectDrink] = useState([]);

  const onChangeDrink = (event) => {
    addDrinkProp(event.target.value);
  };

  const getData = async (url, body) => {
    const response = await fetch(url, body);
    const responseData = response.json();
    return responseData;
  };

  useEffect((data) => {
    getData("http://42.115.221.44:8080/devcamp-pizza365/drinks")
      .then((data) => {
        console.log(data);
        setSelectDrink(data);
      })
      .catch((error) => {
        console.log(error.message);
      });
  }, []);

  return (
    <Row style={{ marginTop: "80px" }}>
      <Col xs={12} className="text-center p-4 mt-4">
        <Typography variant="h4">
          <b className="p-2 border-bottom border-warning text-orange">
            Chọn loại đồ uống
          </b>
        </Typography>
      </Col>

      <Col xs={12}>
        <FormControl fullWidth>
          <InputLabel id="demo-simple-select-helper-label">Đồ uống</InputLabel>
          <Select
            id="select-drink"
            label="Đồ uống"
            defaultValue="None"
            onChange={onChangeDrink}
          >
            <MenuItem value="None">Chưa chọn đồ uống</MenuItem>
            {selectDrink.map((element, index) => {
              return (
                <MenuItem key={index} value={element.maNuocUong}>
                  {element.tenNuocUong}
                </MenuItem>
              );
            })}
          </Select>
        </FormControl>
      </Col>
    </Row>
  );
}

export default Drink;
