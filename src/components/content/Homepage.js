import Slider from "./slider/Slider";
import Info from "./info/Info";
import PizzaSize from "./pizzaSize/PizzaSize";
import PizzaType from "./pizzaType/PizzaType";
import DrinkSelect from "./drinkSelect/DrinkSelect";
import FormUser from "./formUser/FormUser";

import { Container, Row, Col } from "reactstrap";
import { Typography } from "@mui/material";

import seafood from "../../assets/images/seafood.jpg";
import hawaiian from "../../assets/images/hawaiian.jpg";
import bacon from "../../assets/images/bacon.jpg";

import { useState } from "react";

const comboSize = [
  {
    size: "S",
    duongKinh: "20cm",
    suonNuong: "2",
    salad: 200,
    drink: "2",
    prices: 150000,
  },
  {
    size: "M",
    duongKinh: "25cm",
    suonNuong: "4",
    salad: 300,
    drink: "3",
    prices: 200000,
  },
  {
    size: "L",
    duongKinh: "30cm",
    suonNuong: "8",
    salad: 500,
    drink: "4",
    prices: 250000,
  },
];

const type = [
  {
    image: seafood,
    name: "OCEAN MANIA",
    description: "PIZZA HẢI SẢN XỐT MAYONNAISE",
    ingredients:
      "Xốt Cà Chua, Phô Mai Mozzarella Tôm, Mực,Thanh Cua, Hành Tây.",
  },
  {
    image: hawaiian,
    name: "HAWAIIAN",
    description: "PIZZA DĂM BÔNG DỨA KIỂU HAWAII",
    ingredients: "Xốt Cà Chua, Phô Mai Mozzarella Thịt Dăm Bông, Dứa.",
  },
  {
    image: bacon,
    name: "CHEESY CHICKEN BACON",
    description: "PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI",
    ingredients:
      "Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà chua",
  },
];

function HomePage() {
  const [pizzaSize, setPizzaSize] = useState([]);
  const [pizzaType, setPizzaType] = useState([]);
  const [drink, setDrink] = useState("");

  const addPizzaSize = (value) => {
    setPizzaSize(value);
  };

  const addPizzaType = (value) => {
    setPizzaType(value);
  };

  const addDrink = (value) => {
    setDrink(value);
  };

  return (
    <Container style={{ marginTop: "80px", width: "1000px" }}>
      <Row>
        <Col xs={12}>
          <Slider />
          <Info />
          <Row style={{ marginTop: "80px" }}>
            <Col xs={12} className="text-center p-4 mt-4">
              <Typography variant="h4">
                <b className="p-2 border-bottom border-warning text-orange">
                  Chọn Combo Size
                </b>
              </Typography>
            </Col>
            {comboSize.map((pizzaSize, index) => {
              return (
                <Col xs={4} key={index}>
                  <PizzaSize
                    pizzaSizeProp={pizzaSize}
                    addPizzaSize={addPizzaSize}
                  />
                </Col>
              );
            })}
          </Row>

          <Row style={{ marginTop: "80px" }}>
            <Col xs={12} className="text-center p-4 mt-4">
              <Typography variant="h4">
                <b className="p-2 border-bottom border-warning text-orange">
                  Chọn loại Pizza
                </b>
              </Typography>
            </Col>
            {type.map((pizzaType, index) => {
              return (
                <Col xs={4} key={index}>
                  <PizzaType
                    pizzaTypeProp={pizzaType}
                    addPizzaType={addPizzaType}
                  />
                </Col>
              );
            })}
          </Row>

          <DrinkSelect addDrinkProp={addDrink} />
          <FormUser />
        </Col>
      </Row>
      <Typography variant="h6">{pizzaSize.size}</Typography>
      <Typography variant="h6">{pizzaType.name}</Typography>
      <Typography variant="h6">{drink}</Typography>
    </Container>
  );
}

export default HomePage;
