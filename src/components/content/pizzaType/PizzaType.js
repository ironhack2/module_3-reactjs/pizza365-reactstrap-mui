import {
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardText,
} from "reactstrap";
import { Typography, Button } from "@mui/material";

function PizzaType({ pizzaTypeProp, addPizzaType }) {
  const btnPizzaType = () => {
    addPizzaType(pizzaTypeProp);
  };

  return (
    <Card className="bg-card">
      <CardImg
        alt="Card image cap"
        className="card-img-top"
        src={pizzaTypeProp.image}
      />
      <CardBody className="text-white bg-pizza">
        <CardTitle>
          <b>{pizzaTypeProp.name}</b>
        </CardTitle>
        <CardTitle style={{ fontSize: "12px" }}>
          <b>{pizzaTypeProp.description}</b>
        </CardTitle>
        <CardText className="mt-3" style={{ fontSize: "12px" }}>
          {pizzaTypeProp.ingredients}
        </CardText>
        <Row className="mt-3 text-center">
          <Button variant="contained" color="warning" onClick={btnPizzaType}>
            Chọn
          </Button>
        </Row>
      </CardBody>
    </Card>
  );
}

export default PizzaType;
