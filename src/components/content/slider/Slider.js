import { Row, Col, UncontrolledCarousel } from "reactstrap";
import { Typography } from "@mui/material";

import slide1 from "../../../assets/images/1.jpg";
import slide2 from "../../../assets/images/2.jpg";
import slide3 from "../../../assets/images/3.jpg";
import slide4 from "../../../assets/images/4.jpg";

function Info() {
  return (
    <Row>
      <Col xs={12}>
        <Typography variant="h4" className="text-orange">
          Pizza 365
        </Typography>
        <Typography variant="h7">
          <i className="text-orange">Truly italian!</i>
        </Typography>
      </Col>
      <Col xs={12}>
        <UncontrolledCarousel
          className="img-slide"
          items={[
            {
              altText: "Slide 1",
              caption: "",
              key: 1,
              src: slide1,
            },
            {
              altText: "Slide 2",
              caption: "",
              key: 2,
              src: slide2,
            },
            {
              altText: "Slide 3",
              caption: "",
              key: 3,
              src: slide3,
            },
            {
              altText: "Slide 4",
              caption: "",
              key: 4,
              src: slide4,
            },
          ]}
        />
      </Col>
    </Row>
  );
}
export default Info;
