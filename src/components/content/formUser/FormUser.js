import { Container, Row, Col } from "reactstrap";
import { Typography, Button, TextField } from "@mui/material";

function FormUser() {
  return (
    <Row style={{ marginTop: "80px" }} className="bg-taodon">
      <Col xs={12} className="text-center p-4 mt-4">
        <Typography variant="h4">
          <b className="p-2 border-bottom border-warning text-orange">
            Gửi đơn hàng
          </b>
        </Typography>
      </Col>

      <Col xs={10} className="mx-auto">
        <Row>
          <Col xs={12} className="mx-auto mt-3">
            <Row>
              <Col xs={3}>
                <Typography variant="h6">Họ và tên:</Typography>
              </Col>
              <Col xs={9}>
                <TextField
                  fullWidth
                  id="outlined-basic"
                  label="Full Name"
                  variant="outlined"
                />
              </Col>
            </Row>
          </Col>

          <Col xs={12} className="mx-auto mt-3">
            <Row>
              <Col xs={3}>
                <Typography variant="h6">Email:</Typography>
              </Col>
              <Col xs={9}>
                <TextField
                  fullWidth
                  id="outlined-basic"
                  label="Email"
                  variant="outlined"
                />
              </Col>
            </Row>
          </Col>

          <Col xs={12} className="mx-auto mt-3">
            <Row>
              <Col xs={3}>
                <Typography variant="h6">Điện thoại:</Typography>
              </Col>
              <Col xs={9}>
                <TextField
                  fullWidth
                  id="outlined-basic"
                  label="Phone"
                  variant="outlined"
                />
              </Col>
            </Row>
          </Col>

          <Col xs={12} className="mx-auto mt-3">
            <Row>
              <Col xs={3}>
                <Typography variant="h6">Lời nhắn:</Typography>
              </Col>
              <Col xs={9}>
                <TextField
                  fullWidth
                  id="outlined-basic"
                  label="Message"
                  variant="outlined"
                />
              </Col>
            </Row>
          </Col>

          <Col xs={12} className="mx-auto mt-3">
            <Row>
              <Col xs={3}>
                <Typography variant="h6">Mã giãm giá (VoucherID):</Typography>
              </Col>
              <Col xs={9}>
                <TextField
                  fullWidth
                  id="outlined-basic"
                  label="Voucher"
                  variant="outlined"
                />
              </Col>
            </Row>
          </Col>

          <Col xs={12} className="mx-auto mt-5">
            <Row>
              <Button variant="contained" color="warning">
                Gửi đơn
              </Button>
            </Row>
          </Col>
        </Row>
      </Col>
    </Row>
  );
}

export default FormUser;
