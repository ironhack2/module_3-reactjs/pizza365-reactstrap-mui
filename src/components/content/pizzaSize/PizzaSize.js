import { Row, Col, Card, CardImg, CardBody, CardTitle } from "reactstrap";
import { Typography, Button } from "@mui/material";

import suon from "../../../assets/images/suon.jpg";
import salad from "../../../assets/images/salad.jpg";
import nuocngot from "../../../assets/images/nuocngot.jpg";
import seafood from "../../../assets/images/seafood.jpg";

function PizzaSize({ pizzaSizeProp, addPizzaSize }) {
  const btnPizzaSize = () => {
    addPizzaSize(pizzaSizeProp);
  };

  return (
    <Card className="bg-card">
      <Col className="text-center bg-orange">
        <CardTitle className="mt-3">
          <Typography variant="h5">
            <b>{pizzaSizeProp.size}</b>
          </Typography>
        </CardTitle>
      </Col>

      <CardBody className="mt-3">
        <div className="list-group-item ">
          <Row className="bg-light">
            <Col xs={2}>
              <CardImg
                alt="Card image cap"
                className="rounded-circle img-combo"
                src={seafood}
              />
            </Col>
            <Col xs={6}>
              <CardTitle variant="h7">Đường kính: </CardTitle>
            </Col>
            <Col xs={4}>
              <CardTitle variant="h7">
                {" "}
                <b> {pizzaSizeProp.duongKinh} </b>{" "}
              </CardTitle>
            </Col>
          </Row>
        </div>

        <div className="list-group-item ">
          <Row className="bg-light">
            <Col xs={2}>
              <CardImg
                alt="Card image cap"
                className="rounded-circle img-combo"
                src={suon}
              />
            </Col>
            <Col xs={6}>
              <CardTitle variant="h7">Sườn nướng: </CardTitle>
            </Col>
            <Col xs={4}>
              <CardTitle variant="h7">
                {" "}
                <b> {pizzaSizeProp.suonNuong} </b>{" "}
              </CardTitle>
            </Col>
          </Row>
        </div>

        <div className="list-group-item ">
          <Row className="bg-light">
            <Col xs={2}>
              <CardImg
                alt="Card image cap"
                className="rounded-circle img-combo"
                src={salad}
              />
            </Col>
            <Col xs={6}>
              <CardTitle variant="h7">Salad: </CardTitle>
            </Col>
            <Col xs={4}>
              <CardTitle variant="h7">
                {" "}
                <b> {pizzaSizeProp.salad} </b>{" "}
              </CardTitle>
            </Col>
          </Row>
        </div>

        <div className="list-group-item ">
          <Row className="bg-light">
            <Col xs={2}>
              <CardImg
                alt="Card image cap"
                className="rounded-circle img-combo"
                src={nuocngot}
              />
            </Col>
            <Col xs={6}>
              <CardTitle variant="h7">Nước ngọt: </CardTitle>
            </Col>
            <Col xs={4}>
              <CardTitle variant="h7">
                {" "}
                <b> {pizzaSizeProp.drink} </b>{" "}
              </CardTitle>
            </Col>
          </Row>
        </div>

        <Row className="mt-4 text-center">
          <CardTitle variant="h1" className="text-danger">
            <Typography variant="h4">
              <b>{pizzaSizeProp.prices}</b>
            </Typography>
          </CardTitle>
          <CardTitle variant="h3">
            <Typography variant="h5">
              <b>VNĐ</b>
            </Typography>
          </CardTitle>
        </Row>

        <Row className="mt-3 text-center">
          <Button variant="contained" color="warning" onClick={btnPizzaSize}>
            Chọn
          </Button>
        </Row>
      </CardBody>
    </Card>
  );
}

export default PizzaSize;
