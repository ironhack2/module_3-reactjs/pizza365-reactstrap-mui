import { Container, Row, Col, UncontrolledCarousel, CardGroup, Card, CardImg, CardBody, CardTitle, CardText, Carousel } from "reactstrap";
import { Typography, Button, FormControl, InputLabel, Select, MenuItem, TextField } from "@mui/material";
import {useEffect, useState} from 'react';

import slide1 from "../../assets/images/1.jpg";
import slide2 from "../../assets/images/2.jpg";
import slide3 from "../../assets/images/3.jpg";
import slide4 from "../../assets/images/4.jpg";

import whypizza1 from "../../assets/images/whypizza1.jpg";
import whypizza2 from "../../assets/images/whypizza2.jpg";
import whypizza3 from "../../assets/images/whypizza3.jpg";
import whypizza4 from "../../assets/images/whypizza4.jpg";

import suon from "../../assets/images/suon.jpg";
import salad from "../../assets/images/salad.jpg";
import nuocngot from "../../assets/images/nuocngot.jpg";
import seafood from "../../assets/images/seafood.jpg";
import hawaiian from "../../assets/images/hawaiian.jpg";
import bacon from "../../assets/images/bacon.jpg";

function Content() {
    const [selectDrink, setSelectDrink] = useState([]);

    const getData = async (url, body) =>{
        const response = await fetch(url, body);
        const responseData = response.json();
        return responseData;
    }

   useEffect((data)=>{
    getData("http://42.115.221.44:8080/devcamp-pizza365/drinks")
    .then((data)=>{
        console.log(data)
        setSelectDrink(data);
    })
    .catch((error)=>{
        console.log(error.message);
    })
   },[])

    return (
        <Container>

                    {/* SLIDE */}
                    <Row>
                        <Col xs={12}>
                            <Typography variant="h4" className="text-orange">Pizza 365</Typography>
                            <Typography variant="h7"><i className="text-orange">Truly italian!</i></Typography>
                        </Col>
                        <Col xs={12}>
                            <UncontrolledCarousel className="img-slide"
                                items={[
                                    {
                                        altText: 'Slide 1',
                                        caption: '',
                                        key: 1,
                                        src: slide1
                                    },
                                    {
                                        altText: 'Slide 2',
                                        caption: '',
                                        key: 2,
                                        src: slide2
                                    },
                                    {
                                        altText: 'Slide 3',
                                        caption: '',
                                        key: 3,
                                        src: slide3
                                    },
                                    {
                                        altText: 'Slide 4',
                                        caption: '',
                                        key: 4,
                                        src: slide4
                                    }
                                ]}
                            />
                        </Col>
                    </Row>

                    {/* WHY */}
                    <Row style={{ marginTop: "80px" }}>
                        <Col xs={12} className="text-center p-4 mt-4">
                            <Typography variant="h4"><b className="p-2 border-bottom border-warning text-orange">Tại sao lại Pizza 365</b></Typography>
                        </Col>
                        <Col xs={12}>
                            <CardGroup>
                                <Card className="why-green border">
                                    <div className="text-center">
                                        <CardImg alt="Card image cap" className="text-center img-why" src={whypizza1} />
                                    </div>
                                    <CardBody>
                                        <CardTitle variant="h7" className="mt-3 p-2"> <b>Đa dạng</b> </CardTitle>
                                        <CardText className="p-2">
                                            Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay.
                                        </CardText>
                                    </CardBody>
                                </Card>

                                <Card className="why-yellow border">
                                    <div className="text-center">
                                        <CardImg alt="Card image cap" className="text-center img-why" src={whypizza2} />
                                    </div>
                                    <CardBody>
                                        <CardTitle variant="h7" className="mt-3 p-1"> <b>Chất lượng</b> </CardTitle>
                                        <CardText className="p-2">
                                            Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.
                                        </CardText>
                                    </CardBody>
                                </Card>

                                <Card className="why-pink border">
                                    <div className="text-center">
                                        <CardImg alt="Card image cap" className="text-center img-why" src={whypizza3} />
                                    </div>
                                    <CardBody>
                                        <CardTitle variant="h7" className="mt-3 p-2"> <b>Hương vị</b> </CardTitle>
                                        <CardText className="p-2">
                                            Đảm bảo hương vị ngon, độc, lạ, mà bạn chỉ có thể trãi nghiệm từ Pizza 365.
                                        </CardText>
                                    </CardBody>
                                </Card>

                                <Card className="why-orange border">
                                    <div className="text-center">
                                        <CardImg alt="Card image cap" className="text-center img-why" src={whypizza4} />
                                    </div>
                                    <CardBody>
                                        <CardTitle variant="h7" className="mt-3 p-2"> <b>Dịch vụ</b> </CardTitle>
                                        <CardText className="p-2">
                                            Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh, tân tiến.
                                        </CardText>
                                    </CardBody>
                                </Card>
                            </CardGroup>
                        </Col>
                    </Row>


                    {/* COMBO */}
                    <Row style={{ marginTop: "80px" }}>
                        <Col xs={12} className="text-center p-4 mt-4">
                            <Typography variant="h4"><b className="p-2 border-bottom border-warning text-orange">Chọn loại Combo</b></Typography>
                        </Col>
                        <Col xs={12}>
                            <Row>

                                {/* SIZE S */}
                                <Col xs={4}>
                                    <Card className="bg-card">
                                        <Col className="text-center bg-orange">
                                            <CardTitle variant="h3" className="mt-3"> Size S </CardTitle>
                                        </Col>
                                        <CardBody className="mt-3">
                                            <div className="list-group-item ">
                                                <Row className="bg-light">
                                                    <Col xs={2}>
                                                        <CardImg alt="Card image cap" className="rounded-circle img-combo" src={seafood} />
                                                    </Col>
                                                    <Col xs={6}>
                                                        <CardTitle variant="h7">Đường kính: </CardTitle>
                                                    </Col>
                                                    <Col xs={4}>
                                                        <CardTitle variant="h7"> <b> 20cm </b> </CardTitle>
                                                    </Col>
                                                </Row>
                                            </div>

                                            <div className="list-group-item ">
                                                <Row className="bg-light">
                                                    <Col xs={2}>
                                                        <CardImg alt="Card image cap" className="rounded-circle img-combo" src={suon} />
                                                    </Col>
                                                    <Col xs={6}>
                                                        <CardTitle variant="h7">Sườn nướng: </CardTitle>
                                                    </Col>
                                                    <Col xs={4}>
                                                        <CardTitle variant="h7"> <b> 2 pieces </b> </CardTitle>
                                                    </Col>
                                                </Row>
                                            </div>

                                            <div className="list-group-item ">
                                                <Row className="bg-light">
                                                    <Col xs={2}>
                                                        <CardImg alt="Card image cap" className="rounded-circle img-combo" src={salad} />
                                                    </Col>
                                                    <Col xs={6}>
                                                        <CardTitle variant="h7">Salad: </CardTitle>
                                                    </Col>
                                                    <Col xs={4}>
                                                        <CardTitle variant="h7"> <b> 200gr </b> </CardTitle>
                                                    </Col>
                                                </Row>
                                            </div>

                                            <div className="list-group-item ">
                                                <Row className="bg-light">
                                                    <Col xs={2}>
                                                        <CardImg alt="Card image cap" className="rounded-circle img-combo" src={nuocngot} />
                                                    </Col>
                                                    <Col xs={6}>
                                                        <CardTitle variant="h7">Nước ngọt: </CardTitle>
                                                    </Col>
                                                    <Col xs={4}>
                                                        <CardTitle variant="h7"> <b> 2 cup </b> </CardTitle>
                                                    </Col>
                                                </Row>
                                            </div>

                                            <Row className="mt-4 text-center">
                                                <CardTitle variant="h1" className="text-danger">
                                                    <Typography variant="h4"><b>150.000</b></Typography>
                                                </CardTitle>
                                                <CardTitle variant="h3">
                                                    <Typography variant="h5"><b>VNĐ</b></Typography>
                                                </CardTitle>
                                            </Row>

                                            <Row className="mt-3 text-center">
                                                <Button variant="contained" color="warning">Chọn</Button>
                                            </Row>

                                        </CardBody>
                                    </Card>

                                </Col>

                                {/* SIZE M */}
                                <Col xs={4}>
                                    <Card className="bg-card">
                                        <Col className="text-center bg-orange">
                                            <CardTitle variant="h3" className="mt-3"> Size M </CardTitle>
                                        </Col>
                                        <CardBody className="mt-3">
                                            <div className="list-group-item ">
                                                <Row className="bg-light">
                                                    <Col xs={2}>
                                                        <CardImg alt="Card image cap" className="rounded-circle img-combo" src={seafood} />
                                                    </Col>
                                                    <Col xs={6}>
                                                        <CardTitle variant="h7">Đường kính: </CardTitle>
                                                    </Col>
                                                    <Col xs={4}>
                                                        <CardTitle variant="h7"> <b> 25cm </b> </CardTitle>
                                                    </Col>
                                                </Row>
                                            </div>

                                            <div className="list-group-item ">
                                                <Row className="bg-light">
                                                    <Col xs={2}>
                                                        <CardImg alt="Card image cap" className="rounded-circle img-combo" src={suon} />
                                                    </Col>
                                                    <Col xs={6}>
                                                        <CardTitle variant="h7">Sườn nướng: </CardTitle>
                                                    </Col>
                                                    <Col xs={4}>
                                                        <CardTitle variant="h7"> <b> 4 pieces </b> </CardTitle>
                                                    </Col>
                                                </Row>
                                            </div>

                                            <div className="list-group-item ">
                                                <Row className="bg-light">
                                                    <Col xs={2}>
                                                        <CardImg alt="Card image cap" className="rounded-circle img-combo" src={salad} />
                                                    </Col>
                                                    <Col xs={6}>
                                                        <CardTitle variant="h7">Salad: </CardTitle>
                                                    </Col>
                                                    <Col xs={4}>
                                                        <CardTitle variant="h7"> <b> 300gr </b> </CardTitle>
                                                    </Col>
                                                </Row>
                                            </div>

                                            <div className="list-group-item ">
                                                <Row className="bg-light">
                                                    <Col xs={2}>
                                                        <CardImg alt="Card image cap" className="rounded-circle img-combo" src={nuocngot} />
                                                    </Col>
                                                    <Col xs={6}>
                                                        <CardTitle variant="h7">Nước ngọt: </CardTitle>
                                                    </Col>
                                                    <Col xs={4}>
                                                        <CardTitle variant="h7"> <b> 23cup </b> </CardTitle>
                                                    </Col>
                                                </Row>
                                            </div>

                                            <Row className="mt-4 text-center">
                                                <CardTitle variant="h1" className="text-danger">
                                                    <Typography variant="h4"><b>200.000</b></Typography>
                                                </CardTitle>
                                                <CardTitle variant="h3">
                                                    <Typography variant="h5"><b>VNĐ</b></Typography>
                                                </CardTitle>
                                            </Row>

                                            <Row className="mt-3 text-center">
                                                <Button variant="contained" color="warning">Chọn</Button>
                                            </Row>

                                        </CardBody>
                                    </Card>

                                </Col>

                                {/* SIZE L */}
                                <Col xs={4}>
                                    <Card className="bg-card">
                                        <Col className="text-center bg-orange">
                                            <CardTitle variant="h3" className="mt-3"> Size L </CardTitle>
                                        </Col>
                                        <CardBody className="mt-3">
                                            <div className="list-group-item ">
                                                <Row className="bg-light">
                                                    <Col xs={2}>
                                                        <CardImg alt="Card image cap" className="rounded-circle img-combo" src={seafood} />
                                                    </Col>
                                                    <Col xs={6}>
                                                        <CardTitle variant="h7">Đường kính: </CardTitle>
                                                    </Col>
                                                    <Col xs={4}>
                                                        <CardTitle variant="h7"> <b> 30cm </b> </CardTitle>
                                                    </Col>
                                                </Row>
                                            </div>

                                            <div className="list-group-item ">
                                                <Row className="bg-light">
                                                    <Col xs={2}>
                                                        <CardImg alt="Card image cap" className="rounded-circle img-combo" src={suon} />
                                                    </Col>
                                                    <Col xs={6}>
                                                        <CardTitle variant="h7">Sườn nướng: </CardTitle>
                                                    </Col>
                                                    <Col xs={4}>
                                                        <CardTitle variant="h7"> <b> 8 pieces </b> </CardTitle>
                                                    </Col>
                                                </Row>
                                            </div>

                                            <div className="list-group-item ">
                                                <Row className="bg-light">
                                                    <Col xs={2}>
                                                        <CardImg alt="Card image cap" className="rounded-circle img-combo" src={salad} />
                                                    </Col>
                                                    <Col xs={6}>
                                                        <CardTitle variant="h7">Salad: </CardTitle>
                                                    </Col>
                                                    <Col xs={4}>
                                                        <CardTitle variant="h7"> <b> 500gr </b> </CardTitle>
                                                    </Col>
                                                </Row>
                                            </div>

                                            <div className="list-group-item ">
                                                <Row className="bg-light">
                                                    <Col xs={2}>
                                                        <CardImg alt="Card image cap" className="rounded-circle img-combo" src={nuocngot} />
                                                    </Col>
                                                    <Col xs={6}>
                                                        <CardTitle variant="h7">Nước ngọt: </CardTitle>
                                                    </Col>
                                                    <Col xs={4}>
                                                        <CardTitle variant="h7"> <b> 4 cup </b> </CardTitle>
                                                    </Col>
                                                </Row>
                                            </div>

                                            <Row className="mt-4 text-center">
                                                <CardTitle variant="h1" className="text-danger">
                                                    <Typography variant="h4"><b>250.000</b></Typography>
                                                </CardTitle>
                                                <CardTitle variant="h3">
                                                    <Typography variant="h5"><b>VNĐ</b></Typography>
                                                </CardTitle>
                                            </Row>

                                            <Row className="mt-3 text-center">
                                                <Button variant="contained" color="warning">Chọn</Button>
                                            </Row>

                                        </CardBody>
                                    </Card>

                                </Col>
                            </Row>
                        </Col>

                    </Row>


                    {/* PIZZA */}
                    <Row style={{ marginTop: "80px" }}>
                        <Col xs={12} className="text-center p-4 mt-4">
                            <Typography variant="h4"><b className="p-2 border-bottom border-warning text-orange">Chọn loại Pizza</b></Typography>
                        </Col>

                        <Col xs={12}>
                            <Row>
                                <Col xs={4}>
                                    <Card className="bg-card">
                                        <CardImg alt="Card image cap" className="card-img-top" src={hawaiian} />
                                        <CardBody className="text-white bg-pizza">
                                            <CardTitle>
                                                <b>OCEAN MANIA</b>
                                            </CardTitle>
                                            <CardTitle style={{ fontSize: "12px" }}>
                                                <b>PIZZA HẢI SẢN XỐT MAYONNAISE</b>
                                            </CardTitle>
                                            <CardText className="mt-3" style={{ fontSize: "12px" }}>
                                                Xốt Cà Chua, Phô Mai Mozzarella Tôm, Mực,Thanh Cua, Hành Tây.
                                            </CardText>
                                            <Row className="mt-3 text-center">
                                                <Button variant="contained" color="warning">Chọn</Button>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                </Col>

                                <Col xs={4}>
                                    <Card className="bg-card">
                                        <CardImg alt="Card image cap" className="card-img-top" src={seafood} />
                                        <CardBody className="text-white bg-pizza">
                                            <CardTitle> <b>HAWAIIAN</b> </CardTitle>
                                            <CardTitle style={{ fontSize: "12px" }}> <b>PIZZA DĂM BÔNG DỨA KIỂU HAWAII</b> </CardTitle>
                                            <CardText className="mt-3" style={{ fontSize: "12px" }}>
                                                Xốt Cà Chua, Phô Mai Mozzarella Thịt Dăm Bông, Dứa.
                                            </CardText>
                                            <Row className="mt-3 text-center">
                                                <Button variant="contained" color="warning">Chọn</Button>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                </Col>

                                <Col xs={4}>
                                    <Card className="bg-card">
                                        <CardImg alt="Card image cap" className="card-img-top" src={bacon} />
                                        <CardBody className="text-white bg-pizza">
                                            <CardTitle> <b>CHEESY CHICKEN BACON</b> </CardTitle>
                                            <CardTitle style={{ fontSize: "12px" }}> <b>PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</b> </CardTitle>
                                            <CardText className="mt-3" style={{ fontSize: "12px" }}>
                                                Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà Chua.
                                            </CardText>
                                            <Row className="mt-3 text-center">
                                                <Button variant="contained" color="warning">Chọn</Button>
                                            </Row>
                                        </CardBody>
                                    </Card>
                                </Col>
                            </Row>
                        </Col>
                    </Row>


                    {/* DRINK */}
                    <Row style={{ marginTop: "80px" }}>
                        <Col xs={12} className="text-center p-4 mt-4">
                            <Typography variant="h4"><b className="p-2 border-bottom border-warning text-orange">Chọn loại đồ uống</b></Typography>
                        </Col>

                        <Col xs={12}>
                            <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-helper-label">Đồ uống</InputLabel>
                                <Select
                                   id="select-drink"
                                    label="Đồ uống"
                                    defaultValue="None"
                                >
                                    <MenuItem value="None">Chưa chọn đồ uống</MenuItem>
                                     {
                                        selectDrink.map((element, index)=>{
                                            return(
                                                <MenuItem key={index} value={element.maNuocUong}>{element.tenNuocUong}</MenuItem>
                                            )
                                        })
                                     }
                                </Select>
                            </FormControl>
                        </Col>
                    </Row>


                    {/* FORM */}
                    <Row style={{ marginTop: "80px" }} className="bg-taodon">
                        <Col xs={12} className="text-center p-4 mt-4">
                            <Typography variant="h4"><b className="p-2 border-bottom border-warning text-orange">Gửi đơn hàng</b></Typography>
                        </Col>

                        <Col xs={10} className="mx-auto">
                            <Row>
                                <Col xs={12} className="mx-auto mt-3">
                                    <Row>
                                        <Col xs={3}>
                                            <Typography variant="h6">Họ và tên:</Typography>
                                        </Col>
                                        <Col xs={9}>
                                            <TextField fullWidth id="outlined-basic" label="Full Name" variant="outlined" />
                                        </Col>
                                    </Row>
                                </Col>

                                <Col xs={12} className="mx-auto mt-3">
                                    <Row>
                                        <Col xs={3}>
                                            <Typography variant="h6">Email:</Typography>
                                        </Col>
                                        <Col xs={9}>
                                            <TextField fullWidth id="outlined-basic" label="Email" variant="outlined" />
                                        </Col>
                                    </Row>
                                </Col>

                                <Col xs={12} className="mx-auto mt-3">
                                    <Row>
                                        <Col xs={3}>
                                            <Typography variant="h6">Điện thoại:</Typography>
                                        </Col>
                                        <Col xs={9}>
                                            <TextField fullWidth id="outlined-basic" label="Phone" variant="outlined" />
                                        </Col>
                                    </Row>
                                </Col>

                                <Col xs={12} className="mx-auto mt-3">
                                    <Row>
                                        <Col xs={3}>
                                            <Typography variant="h6">Lời nhắn:</Typography>
                                        </Col>
                                        <Col xs={9}>
                                            <TextField fullWidth id="outlined-basic" label="Message" variant="outlined" />
                                        </Col>
                                    </Row>
                                </Col>

                                <Col xs={12} className="mx-auto mt-3">
                                    <Row>
                                        <Col xs={3}>
                                            <Typography variant="h6">Mã giãm giá (VoucherID):</Typography>
                                        </Col>
                                        <Col xs={9}>
                                            <TextField fullWidth id="outlined-basic" label="Voucher" variant="outlined" />
                                        </Col>
                                    </Row>
                                </Col>

                                <Col xs={12} className="mx-auto mt-5">
                                    <Row>
                                        <Button variant="contained" color="warning">Gửi đơn</Button>
                                    </Row>
                                </Col>
                            </Row>
                        </Col>
                    </Row>

        </Container>
    )
}

export default Content