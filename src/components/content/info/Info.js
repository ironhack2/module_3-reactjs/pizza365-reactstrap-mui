import {
  Row,
  Col,
  CardGroup,
  Card,
  CardImg,
  CardBody,
  CardTitle,
  CardText,
} from "reactstrap";
import { Typography } from "@mui/material";

import whypizza1 from "../../../assets/images/whypizza1.jpg";
import whypizza2 from "../../../assets/images/whypizza2.jpg";
import whypizza3 from "../../../assets/images/whypizza3.jpg";
import whypizza4 from "../../../assets/images/whypizza4.jpg";

const info = [
  {
    background: "why-green border",
    name: "Đa dạng",
    description:
      "Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay.",
    image: whypizza1,
  },
  {
    background: "why-yellow border",
    name: "Chất lượng",
    description:
      "Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.",
    image: whypizza2,
  },
  {
    background: "why-pink border",
    name: "Hương vị",
    description:
      "Đảm bảo hương vị ngon, độc, lạ, mà bạn chỉ có thể trãi nghiệm từ Pizza 365.",
    image: whypizza3,
  },
  {
    background: "why-orange border",
    name: "Dịch vụ",
    description:
      "Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh, tân tiến.",
    image: whypizza4,
  },
];

function Info() {
  return (
    <Row style={{ marginTop: "80px" }}>
      <Col xs={12} className="text-center p-4 mt-4">
        <Typography variant="h4">
          <b className="p-2 border-bottom border-warning text-orange">
            Tại sao lại Pizza 365
          </b>
        </Typography>
      </Col>
      <Col xs={12}>
        <CardGroup>
          {info.map((info, index) => {
            return (
              <Card className={info.background} key={index}>
                <div className="text-center">
                  <CardImg
                    alt="Card image cap"
                    className="text-center img-why"
                    src={info.image}
                  />
                </div>
                <CardBody>
                  <CardTitle variant="h7" className="mt-3 p-2">
                    {" "}
                    <b>{info.name}</b>{" "}
                  </CardTitle>
                  <CardText className="p-2">{info.description}</CardText>
                </CardBody>
              </Card>
            );
          })}
        </CardGroup>
      </Col>
    </Row>
  );
}
export default Info;
