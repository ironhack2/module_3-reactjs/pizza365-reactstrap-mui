import { Container, Row, Col } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faPushed,
  faFacebookF,
  faInstagram,
  faSnapchat,
  faPinterest,
  faTwitter,
  faLinkedin,
} from "@fortawesome/free-brands-svg-icons";

function Footer() {
  return (
    <div className="bg-orange mt-5">
      <Container className="bg-orange">
        <Row className="text-center w-100" expand="md">
          <Col className="col-sm-12">
            <h4 className="mt-5">Footer</h4>
            <a href="#" className="btn btn-dark m-3">
              <FontAwesomeIcon icon={faPushed} /> &nbsp; To the top
            </a>
            <div className="m-2">
              <a className="iconfooter" href="https://www.facebook.com/">
                <FontAwesomeIcon icon={faFacebookF} />
              </a>
              <a className="iconfooter" href="https://www.instagram.com/">
                <FontAwesomeIcon icon={faInstagram} />
              </a>
              <a className="iconfooter" href="https://www.snapchat.com/vi-VN">
                <FontAwesomeIcon icon={faSnapchat} />
              </a>
              <a className="iconfooter" href="https://www.pinterest.com/">
                <FontAwesomeIcon icon={faPinterest} />
              </a>
              <a className="iconfooter" href="https://twitter.com/">
                <FontAwesomeIcon icon={faTwitter} />
              </a>
              <a className="iconfooter" href="https://www.linkedin.com/">
                <FontAwesomeIcon icon={faLinkedin} />
              </a>
            </div>
            <div className="m-2">
              <p>Powered by DEVCAMP</p>
            </div>
          </Col>
        </Row>
      </Container>
    </div>
  );
}

export default Footer;
