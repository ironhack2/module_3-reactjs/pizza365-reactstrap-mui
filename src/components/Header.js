import { useState } from "react";
import {
  Container,
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";

function Header() {
  const [toggle, setToggle] = useState(false);

  const openToggle = () => {
    setToggle(!toggle);
  };

  return (
    <Container>
      <Navbar
        className="navbar fixed-top navbar-nav nav-fill w-100 bg-orange"
        light
        expand="md"
      >
        <NavbarBrand href="/"></NavbarBrand>
        <NavbarToggler onClick={openToggle} />
        <Collapse navbar isOpen={toggle}>
          <Nav navbar className="col-sm-12">
            <NavItem className="col-sm-2">
              <NavLink style={{ color: "black" }} className="" href="#">
                Trang chủ
              </NavLink>
            </NavItem>
            <NavItem className="col-sm-2">
              <NavLink style={{ color: "black" }} className="" href="#">
                Combo
              </NavLink>
            </NavItem>
            <NavItem className="col-sm-2">
              <NavLink style={{ color: "black" }} className="" href="#">
                Loại Pizza
              </NavLink>
            </NavItem>
            <NavItem className="col-sm-2">
              <NavLink style={{ color: "black" }} className="" href="#">
                Gửi đơn hàng
              </NavLink>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </Container>
  );
}

export default Header;
