const initialState = {
  pizzaType: [],
  pizzaSize: [],
  drink: "",
};

const TaskEvent = (state = initialState, action) => {
  switch (action.type) {
    case "PIZZA_TYPE": {
      return {
        ...state,
        pizzaType: [...action.pizzaTypes],
      };
    }

    default: {
      return state;
    }
  }
};

export default TaskEvent;
